# Idées
- [ ] Améliorer l'accessibilité
- [x] Proposer la normalisation des pistes enregistrées au moment de la conversion
- [x] Proposer un mode "pro" qui permettrait d'importer des sons de l'appareil
- [x] Ajouter la possiblité de découper un son