// ====================
//Configuration et initialisation
// ====================

// Sélecteurs
const recordButton = document.querySelector('.record');
const stopButton = document.querySelector('.stop');
const soundClips = document.querySelector('.sound-clips');
const canvas = document.querySelector('.visualizer');
const combineButton = document.querySelector('.combine');
const timerDisplay = document.createElement('div');

// Mode pro
const proModeToggle = document.querySelector('.pro-mode-toggle');

//Importation
const importButton = document.querySelector('.import-button');
const fileInput = document.querySelector('.file-input');

const importOptions = document.createElement('div');
importOptions.className = 'import-options';

// Création des boutons pour les 2 options
const importFromComputerButton = document.createElement('button');
importFromComputerButton.textContent = 'Importer depuis mon ordinateur';
importFromComputerButton.className = 'import-option-btn';

const importFromLibraryButton = document.createElement('button');
importFromLibraryButton.textContent = 'Sélectionner depuis la bibliothèque';
importFromLibraryButton.className = 'import-option-btn';
//importFromLibraryButton.disabled = true; // bouton desactivé en attendant
//importFromLibraryButton.style.cursor = "not-allowed"; // bouton desactivé en attendant


// Ajout des boutons au conteneur d'options
importOptions.appendChild(importFromComputerButton);
importOptions.appendChild(importFromLibraryButton);



// Création de la barre de progression
const progressContainer = document.createElement('div');
const progressBar = document.createElement('div');
progressContainer.className = 'progress-container';
progressBar.className = 'progress-bar';
progressContainer.appendChild(progressBar);


soundClips.parentNode.insertBefore(progressContainer, soundClips);

// Oscilloscope
function visualize(stream) {
    if(!audioCtx) {
        audioCtx = new AudioContext();
    }

    const source = audioCtx.createMediaStreamSource(stream);

    const analyser = audioCtx.createAnalyser();
    analyser.fftSize = 2048;
    const bufferLength = analyser.frequencyBinCount;
    const dataArray = new Uint8Array(bufferLength);

    source.connect(analyser);

    draw();

    function draw() {
        const WIDTH = canvas.width;
        const HEIGHT = canvas.height;

        requestAnimationFrame(draw);

        analyser.getByteTimeDomainData(dataArray);

        canvasCtx.fillStyle = 'rgb(200, 200, 200)';
        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

        canvasCtx.lineWidth = 2;
        canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

        canvasCtx.beginPath();

        const sliceWidth = WIDTH * 1.0 / bufferLength;
        let x = 0;

        for(let i = 0; i < bufferLength; i++) {
            const v = dataArray[i] / 128.0;
            const y = v * HEIGHT/2;

            if(i === 0) {
                canvasCtx.moveTo(x, y);
            } else {
                canvasCtx.lineTo(x, y);
            }

            x += sliceWidth;
        }

        canvasCtx.lineTo(canvas.width, canvas.height/2);
        canvasCtx.stroke();
    }
}

// Fonction de mise à jour de la barre de progression
function updateProgressBar(percentage) {
    progressBar.style.width = percentage + '%';
    if (percentage > 0) {
        progressContainer.classList.add('active');
    } else {
        progressContainer.classList.remove('active');
    }
}

// Fonction pour découper l'audio
async function splitAudioAtTime(audioElement, splitTime) {
    try {
        // Afficher la progression
        progressContainer.style.display = 'block';
        updateProgressBar(20);

        // Convertir d'abord en MP3 si ce n'est pas déjà le cas
        let audioBlob;
        if (!audioElement.src.includes('.mp3')) {
            audioBlob = await convertToMp3(audioElement, progress => {
                updateProgressBar(20 + progress * 0.2);
            });
        } else {
            const response = await fetch(audioElement.src);
            audioBlob = await response.blob();
        }

        updateProgressBar(40);

        // Convertir le blob en ArrayBuffer
        const arrayBuffer = await audioBlob.arrayBuffer();
        
        // Décoder l'audio
        const audioContext = new AudioContext();
        const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
        updateProgressBar(60);
        
        // Créer les deux parties
        const splitIndex = Math.floor(splitTime * audioBuffer.sampleRate);
        
        // Première partie
        const firstBuffer = audioContext.createBuffer(
            1,
            splitIndex,
            audioBuffer.sampleRate
        );
        firstBuffer.copyToChannel(audioBuffer.getChannelData(0).slice(0, splitIndex), 0);
        
        // Deuxième partie
        const secondBuffer = audioContext.createBuffer(
            1,
            audioBuffer.length - splitIndex,
            audioBuffer.sampleRate
        );
        secondBuffer.copyToChannel(audioBuffer.getChannelData(0).slice(splitIndex), 0);
        
        updateProgressBar(80);
        
        // Convertir les buffers en MP3
        const firstBlob = await optimizedMp3Encode(
            firstBuffer.getChannelData(0),
            firstBuffer.sampleRate,
            1
        );
        
        const secondBlob = await optimizedMp3Encode(
            secondBuffer.getChannelData(0),
            secondBuffer.sampleRate,
            1
        );
        
        updateProgressBar(100);
        
        return [firstBlob, secondBlob];
    } catch (error) {
        console.error('Erreur détaillée lors du découpage:', error);
        throw error;
    } finally {
        progressContainer.style.display = 'none';
        updateProgressBar(0);
    }
}

// Fonction pour générer des noms pour les parties découpées
function generateSplitNames(baseName) {
    const lowerBaseName = baseName.toLowerCase();
    let prefix = baseName;
    let currentPart = 1;

    // Vérifier si le nom contient déjà "partie X" ou "part X"
    const partMatch = lowerBaseName.match(/(?:partie|part)\s*(\d+)/i);
    if (partMatch) {
        // Si oui, extraire le numéro de partie et l'incrémenter
        currentPart = parseInt(partMatch[1]);
        prefix = baseName.replace(/(?:partie|part)\s*\d+/i, '').trim();
    }

    // Construire les nouveaux noms
    const firstPartName = `${prefix} Partie ${currentPart}`;
    const secondPartName = `${prefix} Partie ${currentPart + 1}`;

    return [firstPartName, secondPartName];
}


// Bouton de normalisation
const normalizeButton = document.createElement('button');
normalizeButton.textContent = 'Combiner et Normaliser';
normalizeButton.className = 'normalize';
// Cacher le bouton de normalisation par défaut
normalizeButton.classList.add('hidden');
normalizeButton.style.display = 'none';

// Affichage du mode pro
let isProMode = false;
proModeToggle.addEventListener('click', () => {
    isProMode = !isProMode;
    proModeToggle.classList.toggle('pro-mode-active', isProMode);
    proModeToggle.textContent = isProMode ? '➖' : '➕';
    importButton.classList.toggle('visible', isProMode);
    importButton.disabled = !isProMode;
    // Afficher/masquer le bouton de normalisation en fonction du mode pro
    normalizeButton.style.display = isProMode ? 'inline-block' : 'none';
    normalizeButton.classList.toggle('visible', isProMode);

    importFromComputerButton.disabled = !isProMode;
    importFromLibraryButton.disabled = !isProMode;
    
    // Si le mode Pro est désactivé et que les options sont visibles, les cacher
    if (!isProMode && importOptions.style.display !== 'none') {
        importOptions.style.display = 'none';
    }
    
    const splitButtons = document.querySelectorAll('.split');
    splitButtons.forEach(button => {
        // Afficher le bouton si nous sommes en mode pro et que l'audio est en pause
        const audio = button.parentNode.querySelector('audio');
        if (isProMode && audio && !audio.paused && audio.currentTime > 0) {
            button.style.display = 'inline-block';
        } else if (!isProMode) {
            button.style.display = 'none';
        }
    });
});


timerDisplay.className = 'timer-display';
timerDisplay.style.display = 'none';

let startTime;
let timerInterval;
let recordingDuration = 0;

// Optimisation selon la plateforme
function getSupportedMimeType() {
    // ajout 16/03 ogg
    if (MediaRecorder.isTypeSupported('audio/ogg; codecs=opus')) {
        console.log('OGG/Opus supporté - utilisation prioritaire');
        return 'audio/ogg; codecs=opus';
    }
    
    const isFirefox = navigator.userAgent.toLowerCase().includes('firefox');
    const isMobile = /mobile|tablet|android/i.test(navigator.userAgent);
  
    
    
    if (isFirefox && isMobile && MediaRecorder.isTypeSupported('audio/webm')) {
        console.log('Firefox mobile détecté - utilisation de WebM');
        return 'audio/webm';
    }
    
    // Les formats de base, avec ogg/opus en premier pour Firefox
    let possibleTypes = [
        'audio/ogg; codecs=opus',
        'audio/webm',
        'audio/mp4',
        'audio/aac'
    ];
    
    // Pour iOS, on privilégie mp4/aac
    if (/iPad|iPhone|iPod/.test(navigator.userAgent)) {
        possibleTypes = [
            'audio/mp4',
            'audio/aac',
            'audio/webm',
            'audio/ogg; codecs=opus'
        ];
    }
    
    // Test de support des formats
    for (const type of possibleTypes) {
        try {
            if (MediaRecorder.isTypeSupported(type)) {
                console.log(`Type MIME utilisé ${type}`);
                return type;
            }
        } catch (e) {
            console.warn(`Erreur lors du test du type ${type}:`, e);
        }
    }
    
    // Fallback pour iOS
    if (/iPad|iPhone|iPod/.test(navigator.userAgent)) {
        return 'audio/mp4';
    }
    
    // Fallback pour Firefox mobile - utiliser le format ogg par défaut
    if (isFirefox && isMobile) {
        return 'audio/ogg; codecs=opus';
    }
    
    throw new Error('Aucun format audio supporté n\'a été trouvé');
}

function getOptimalAudioSettings(mimeType) {
    const isMobile = /mobile|tablet|android/i.test(navigator.userAgent);
    const isFirefox = navigator.userAgent.toLowerCase().includes('firefox');
    
    // Paramètres spécifiques pour Firefox mobile
    if (isFirefox && isMobile && mimeType.includes('opus')) {
        return {
            audioBitsPerSecond: 96000,
            sampleRate: 44100,
            channelCount: 1
        };
    }

    const settings = {
        'audio/mp4': {
            audioBitsPerSecond: 96000,
            sampleRate: 44100,
            channelCount: 1
        },
        'audio/ogg; codecs=opus': {
            audioBitsPerSecond: isMobile ? 96000 : 128000,
            sampleRate: 48000,
            channelCount: 1
        },
        'audio/webm': {
            audioBitsPerSecond: isMobile ? 96000 : 128000,
            sampleRate: 48000,
            channelCount: 1
        },
        'audio/aac': {
            audioBitsPerSecond: 96000,
            sampleRate: 44100,
            channelCount: 1
        }
    };

    return settings[mimeType] || {
        audioBitsPerSecond: isMobile ? 96000 : 128000,
        sampleRate: 44100,
        channelCount: 1
    };
}

// Fonction utilitaire pour déboguer les formats supportés
function debugSupportedFormats() {
    const formats = [
        'audio/ogg; codecs=opus',
        'audio/webm',
        'audio/mp4',
        'audio/aac'
    ];
    
    console.log('Formats supportés :');
    formats.forEach(format => {
        try {
            const isSupported = MediaRecorder.isTypeSupported(format);
            console.log(`${format}: ${isSupported}`);
        } catch (e) {
            console.log(`${format}: erreur lors du test`);
        }
    });
}

// Conteneur pour le bouton combine
const combineButtonContainer = document.createElement('div');
combineButtonContainer.style.textAlign = 'center';
//combineButtonContainer.style.margin = '20px 0';
combineButtonContainer.appendChild(combineButton);
combineButtonContainer.appendChild(normalizeButton);

// Section des clips combinés
const combinedClipsContainer = document.createElement('div');
combinedClipsContainer.className = 'combined-clips';
const combinedTitle = document.createElement('h3');
combinedTitle.textContent = 'Clips Combinés';
combinedTitle.style.marginTop = '20px';
combinedClipsContainer.appendChild(combinedTitle);

// Containers de la page
soundClips.parentNode.insertBefore(timerDisplay, soundClips);
soundClips.parentNode.insertBefore(combineButtonContainer, soundClips.nextSibling);
soundClips.parentNode.insertBefore(combinedClipsContainer, combineButtonContainer.nextSibling);

// Désactiver le bouton stop au démarrage
stopButton.disabled = true;

// Configuration du visualiseur
let audioCtx;
const canvasCtx = canvas.getContext("2d");

// Fonction pour formater le temps
function formatTime(ms) {
    const seconds = Math.floor(ms / 1000);
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds.toString().padStart(2, '0')}`;
}

// Fonction pour le décompte
function startCountdown() {
    return new Promise((resolve) => {
        importButton.disabled= true;
        normalizeButton.disabled= true;
        combineButton.disabled= true;
        recordButton.disabled= true;
        let count = 3;
        const countdownEl = document.createElement('div');
        countdownEl.className = 'countdown';
        document.body.appendChild(countdownEl);
        const countInterval = setInterval(() => {
            if (count > 0) {
                countdownEl.textContent = count;
                count--;
            } else {
                clearInterval(countInterval);
                countdownEl.remove();
                //recordButton.textContent = "Enregistrement en cours";
                resolve();
            }
        }, 1000);
        countdownEl.textContent = count;
    });
}

// Fonction pour démarrer le timer d'enregistrement
function startRecordingTimer() {
    startTime = Date.now();
    timerDisplay.style.display = 'block';
    timerDisplay.textContent = '0:00';
    
    timerInterval = setInterval(() => {
        const currentTime = Date.now();
        recordingDuration = currentTime - startTime;
        timerDisplay.textContent = formatTime(recordingDuration);
    }, 1000);
}

// Fonction pour arrêter le timer
function stopRecordingTimer() {
    clearInterval(timerInterval);
    timerDisplay.style.display = 'none';
    recordingDuration = 0;
}

// Détection du format audio
function detectAudioFormat(audioElement) {
    const sourceUrl = audioElement.src;
    if (sourceUrl.includes('blob:')) {
        return audioElement.type || 'audio/wav'; // Par défaut WAV si non spécifié
    }
    
    // Pour les URLs, on vérifie l'extension
    if (sourceUrl.endsWith('.ogg')) return 'audio/ogg';
    if (sourceUrl.endsWith('.webm')) return 'audio/webm';
    if (sourceUrl.endsWith('.m4a') || sourceUrl.endsWith('.aac')) return 'audio/aac';
    if (sourceUrl.endsWith('.mp3')) return 'audio/mp3';
    return 'audio/wav'; // Format par défaut
}

// Encodage MP3 lamejs
if (typeof lamejs === 'undefined') {
    throw new Error('La bibliothèque lamejs n\'est pas chargée');
} else {console.log('Bibliothèque lamejs chargée')}

async function optimizedMp3Encode(audioData, sampleRate, channels, progressCallback) {
    if (!window.lamejs) {
        throw new Error('La bibliothèque lamejs n\'est pas disponible');
    }

    try {
        const mp3encoder = new lamejs.Mp3Encoder(channels, sampleRate, 128);
        const mp3Data = [];
        const samples = new Int16Array(audioData.length);
        
        // Conversion et normalisation des données audio
        for (let i = 0; i < audioData.length; i++) {
            const sample = Math.max(-1, Math.min(1, audioData[i]));
            samples[i] = sample < 0 ? sample * 0x8000 : sample * 0x7FFF;
        }

        // Traitement par blocs
        const blockSize = 1152;
        const numBlocks = Math.ceil(samples.length / blockSize);

        for (let i = 0; i < numBlocks; i++) {
            if (progressCallback) {
                progressCallback((i / numBlocks) * 100);
            }

            const start = i * blockSize;
            const end = Math.min(start + blockSize, samples.length);
            const leftChunk = samples.slice(start, end);
            
            // Padding si nécessaire
            if (leftChunk.length < blockSize) {
                const paddedChunk = new Int16Array(blockSize);
                paddedChunk.set(leftChunk);
                const mp3buf = mp3encoder.encodeBuffer(paddedChunk);
                if (mp3buf.length > 0) mp3Data.push(mp3buf);
            } else {
                const mp3buf = mp3encoder.encodeBuffer(leftChunk);
                if (mp3buf.length > 0) mp3Data.push(mp3buf);
            }

            // Pause périodique
            if (i % 10 === 0) {
                await new Promise(resolve => setTimeout(resolve, 0));
            }
        }

        const mp3buf = mp3encoder.flush();
        if (mp3buf.length > 0) {
            mp3Data.push(mp3buf);
        }

        return new Blob(mp3Data, { type: 'audio/mp3' });
    } catch (error) {
        console.error('Erreur lors de l\'encodage MP3:', error);
        throw error;
    }
}


// Conversion MP3
async function convertToMp3(audioElement, progressCallback) {
    const audioContext = new (window.AudioContext || window.webkitAudioContext)();
    
    try {
        // Récupération des données audio
        const response = await fetch(audioElement.src);
        const arrayBuffer = await response.arrayBuffer();
        const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
        
        // Extraction des données en mono
        const channelData = audioBuffer.getChannelData(0);
        
        // Encodage MP3
        return await optimizedMp3Encode(
            channelData,
            audioBuffer.sampleRate,
            1, // Force mono
            progressCallback
        );
        
    } catch (error) {
        console.error('Erreur lors de la conversion:', error);
        throw error;
    } finally {
        await audioContext.close();
    }
}
// ======================
// Boutons icones
// =====================
// Fonction utilitaire pour créer une icône
function createIcon(imageName) {
    const img = document.createElement('img');
    img.src = `icones/${imageName}.svg`;
    img.alt = imageName;
    img.className = 'button-icon';
    img.style.pointerEvents = 'none';
    return img;
}

// Fonction pour remplacer le texte des boutons par des icônes
function replaceButtonTextsWithIcons() {
    // Mapping des boutons avec leurs icônes correspondantes
    const buttonIconMap = {
        'Enregistrer': 'microphone',
        'Stop': 'stop3',
        'Supprimer': 'trash2',
        '↑': 'up',
        '↓': 'down',
        'Télécharger': 'download',
        'Découper ici': 'cut2',
        'Découpage en cours...': 'cut2',
        'Combiner': 'combine2',
        'Combiner et Normaliser': 'normalize',
        'Importer un son' : 'upload',
        'Enregistrement en cours' : 'recording',
        'Sélectionner depuis la bibliothèque':'cloud',
        'Importer depuis mon ordinateur':'pc',

    };

    // Sélectionner tous les boutons de l'application
    const buttons = document.querySelectorAll('button');

    buttons.forEach(button => {
        const buttonText = button.textContent.trim();
        if (buttonIconMap[buttonText]) {
            // Vider le contenu textuel du bouton
            button.textContent = '';
            // Ajouter l'icône
            button.appendChild(createIcon(buttonIconMap[buttonText]));
            // Ajouter un titre au survol
            button.title = buttonText;
            

        }
    });
}

// Appeler la fonction au chargement de la page
document.addEventListener('DOMContentLoaded', replaceButtonTextsWithIcons);

// Observer les nouveaux boutons ajoutés dynamiquement
const observer = new MutationObserver(mutations => {
    mutations.forEach(mutation => {
        mutation.addedNodes.forEach(node => {
            if (node.nodeType === 1) { // Element node
                const buttons = node.getElementsByTagName('button');
                if (buttons.length > 0) {
                    replaceButtonTextsWithIcons();
                }
            }
        });
    });
});


observer.observe(document.body, {
    childList: true,
    subtree: true
});

// ====================
// Enregistrement audio
// ====================

if (navigator.mediaDevices.getUserMedia) {
    console.log('getUserMedia supporté.');
    const constraints = { audio: true };

    let onSuccess = function(stream) {
        let mimeType;
        try {
            mimeType = getSupportedMimeType();
        } catch (e) {
            console.error(e);
            alert('Votre navigateur ne supporte pas l\'enregistrement audio.');
            return;
        }

        const audioSettings = getOptimalAudioSettings(mimeType);
        const mediaRecorder = new MediaRecorder(stream, {
            mimeType: mimeType,
            ...audioSettings
        });

        visualize(stream);

        let chunks = [];

        recordButton.onclick = async function() {
            await startCountdown();
            mediaRecorder.start();
            console.log(mediaRecorder.state);
            console.log("démarrage enregistrement");
            stopButton.disabled = false;
            //recordButton.disabled = true;
            importButton.disabled= true;
            normalizeButton.disabled= true;
            combineButton.disabled= true;
            recordButton.classList.add('recording');
            startRecordingTimer();
        };

        stopButton.onclick = function() {
            mediaRecorder.stop();
            console.log(mediaRecorder.state);
            console.log("arrêt enregistrement");
            //recordButton.textContent = "Enregistrer";
            //insertSvgIcon('microphone-solid', recordButton);
            stopButton.disabled = true;
            recordButton.disabled = false;
            importButton.disabled= false;
            normalizeButton.disabled= false;
            combineButton.disabled= false;
            recordButton.classList.remove('recording');
            stopRecordingTimer();
        };

        mediaRecorder.onstop = function(e) {
            console.log("Données disponibles après l'arrêt de de MediaRecorder");
        
            const blob = new Blob(chunks, { type: mimeType });
            chunks = [];
            
            const clipName = prompt('Entrez un nom pour votre clip audio', 'sansNom');
            const clipContainer = document.createElement('article');
            const clipLabel = document.createElement('p');
            const audio = document.createElement('audio');
            const deleteButton = document.createElement('button');
            const downloadButton = document.createElement('button');
            const moveUpButton = document.createElement('button');
            const moveDownButton = document.createElement('button');
            const splitButton = document.createElement('button'); // Ajout du bouton de découpage
        
            clipContainer.classList.add('clip');
            audio.setAttribute('controls', '');
            deleteButton.textContent = 'Supprimer';
            deleteButton.className = 'delete';
            downloadButton.textContent = 'Télécharger';
            downloadButton.className = 'download';
            moveUpButton.textContent = '↑';
            moveUpButton.className = 'move-up';
            moveDownButton.textContent = '↓';
            moveDownButton.className = 'move-down';
            splitButton.textContent = 'Découper ici';
            splitButton.className = 'split';
            splitButton.style.display = 'none'; // Caché par défaut
        
            clipLabel.textContent = clipName || 'sansNom';
        
            // Gestion de l'affichage du bouton de découpage
            audio.addEventListener('pause', () => {
                if (isProMode && audio.currentTime > 0 && audio.currentTime < audio.duration) {
                    splitButton.style.display = 'inline-block';
                    splitButton.disabled = false;
                    splitButton.dataset.splitTime = audio.currentTime;
                }
            });
            
            audio.addEventListener('play', () => {
                splitButton.style.display = 'none';
                splitButton.disabled = true;
            });
            
            // Mettre à jour la section du code qui gère le découpage
splitButton.addEventListener('click', async () => {
    const splitTime = parseFloat(splitButton.dataset.splitTime);

    try {
        splitButton.disabled = true;
        //splitButton.textContent = 'Découpage en cours...';

        const [firstPart, secondPart] = await splitAudioAtTime(audio, splitTime);

        const baseName = clipLabel.textContent;
        const [firstPartName, secondPartName] = generateSplitNames(baseName);

        createAudioClip(firstPart, `${firstPartName}.mp3`);
        createAudioClip(secondPart, `${secondPartName}.mp3`);

        clipContainer.remove();

    } catch (error) {
        console.error('Erreur lors du découpage:', error);
        alert('Erreur lors du découpage de l\'audio');
    } finally {
        splitButton.disabled = false;
        splitButton.textContent = 'Découper ici';
    }
});
            
        
            clipContainer.appendChild(moveUpButton);
            clipContainer.appendChild(moveDownButton);
            clipContainer.appendChild(clipLabel);
            clipContainer.appendChild(audio);
            clipContainer.appendChild(splitButton);
            clipContainer.appendChild(deleteButton);
            clipContainer.appendChild(downloadButton);
            soundClips.appendChild(clipContainer);
        
            const audioURL = window.URL.createObjectURL(blob);
            audio.src = audioURL;
        
            deleteButton.onclick = function(e) {
                e.target.parentNode.remove();
            };
        
            clipLabel.onclick = function() {
                const newClipName = prompt('Entrez un nouveau nom pour votre clip audio');
                if(newClipName !== null) {
                    this.textContent = newClipName;
                }
            };
        
            downloadButton.onclick = function() {
                const extension = mimeType.includes('mp4') || mimeType.includes('aac') ? 'm4a' : 
                                mimeType.includes('webm') ? 'webm' : 'ogg';
                const anchor = document.createElement('a');
                anchor.href = audioURL;
                anchor.download = `${clipLabel.textContent}.${extension}`;
                anchor.click();
            };
        
            moveUpButton.onclick = function() {
                const previousSibling = clipContainer.previousElementSibling;
                if (previousSibling) {
                    soundClips.insertBefore(clipContainer, previousSibling);
                }
            };
        
            moveDownButton.onclick = function() {
                const nextSibling = clipContainer.nextElementSibling;
                if (nextSibling) {
                    soundClips.insertBefore(nextSibling, clipContainer);
                }
            };
        };   

        mediaRecorder.ondataavailable = function(e) {
            chunks.push(e.data);
        };
    };

    let onError = function(err) {
        console.log('L\'erreur suivante s\'est produite : ' + err);
    };

    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

} else {
    console.log('getUserMedia n\'est pas supporté sur votre navigateur !');
}


// Fonctionnalité import

// On ajoute le conteneur d'options après le bouton d'import original
importButton.parentNode.insertBefore(importOptions, importButton.nextSibling);
importOptions.style.display = 'none'; // Caché par défaut

// à enlever quand ce sera bon
//importFromLibraryButton.disabled = true;
//importFromLibraryButton.style.opacity = "0.5";
//importFromLibraryButton.style.cursor = "not-allowed";

importButton.addEventListener('click', () => {
    //Vérifier si le mode pro est activé
    if (!isProMode) {
        // Optionnel : afficher un message indiquant que la fonctionnalité nécessite le mode Pro
        //alert('Cette fonctionnalité nécessite le mode Pro.');
        return;
    }


    // Afficher/cacher les options d'importation
    if (importOptions.style.display === 'none') {
        importOptions.style.display = 'flex';
    } else {
        importOptions.style.display = 'none';
    }
});

importFromComputerButton.addEventListener('click', () => {
    if (!isProMode) return; 
    fileInput.click();
    //importOptions.style.display = 'none';
});

// Bouton librairie

const libraryModal = document.createElement('div');
libraryModal.className = 'library-modal';
libraryModal.innerHTML = `
    <div class="library-content">
        <h3>Bibliothèque de sons</h3>
        <div class="library-source-info">Source: <span id="sound-source-info">Bibliothèque complète</span></div>
        
        <div class="library-filters">
            <label>Filtrer par catégorie:</label>
            <select id="category-filter">
                <option value="all">Tous les sons</option>
                <option value="générique">Génériques</option>
                <option value="jingle">Jingles</option>
                <option value="virgule">Virgules</option>
                <option value="divers">Sons divers</option>
            </select>
        </div>
        
        <div class="library-sounds-container"></div>
        <button class="close-library-btn">Fermer</button>
    </div>
`;
document.body.appendChild(libraryModal);

// Comportement du bouton d'import depuis la bibliothèque
importFromLibraryButton.addEventListener('click', () => {
    if (!isProMode) return; 
    //if (!isProMode || importFromLibraryButton.disabled) return; // bouton désactivé en attendant
    loadLibrarySounds();
    libraryModal.style.display = 'flex';
    //importOptions.style.display = 'none';
});

// Fermer la modal de la bibliothèque
document.querySelector('.close-library-btn').addEventListener('click', () => {
    libraryModal.style.display = 'none';
    stopAllPreviews();
});

// Ajouter un écouteur pour le filtre par catégorie
document.querySelector('#category-filter').addEventListener('change', function() {
    loadLibrarySounds(this.value);
});

// Fonction pour charger les sons de la bibliothèque
async function loadLibrarySounds(categoryFilter = 'all') {
    const libraryContainer = document.querySelector('.library-sounds-container');
    libraryContainer.innerHTML = '<p>Chargement des sons...</p>';
    
    try {
        // Charger la liste des sons disponibles
        const response = await fetch('librairie/sounds-list.json');
        const allSounds = await response.json();
        
        // Filtrer les sons par catégorie si nécessaire
        const soundsList = categoryFilter === 'all' 
            ? allSounds 
            : allSounds.filter(sound => sound.category === categoryFilter);
        
        // Mettre à jour l'info de source si un filtre est appliqué
        const sourceInfo = document.getElementById('sound-source-info');
        if (categoryFilter === 'all') {
            sourceInfo.textContent = 'Bibliothèque complète';
        } else {
            sourceInfo.textContent = `Catégorie: ${categoryFilter}`;
        }
        
        // Vider le conteneur
        libraryContainer.innerHTML = '';
        
        // Afficher un message si aucun son ne correspond au filtre
        if (soundsList.length === 0) {
            libraryContainer.innerHTML = '<p>Aucun son trouvé dans cette catégorie.</p>';
            return;
        }
        
        // Créer un élément pour chaque son
        soundsList.forEach(sound => {
            const soundElement = document.createElement('div');
            soundElement.className = 'library-sound-item';
            soundElement.innerHTML = `
                <div class="sound-info">
                    <span class="sound-name">${sound.name}</span>
                    <div class="sound-details">
                        <span class="sound-duration">${sound.duration}</span>
                        <span class="sound-category">${sound.category}</span>
                        <span class="sound-source">${sound.source || 'Source inconnue'}</span>
                    </div>
                </div>
                <div class="sound-controls">
                    <button class="preview-sound-btn" data-src="${sound.src}">
                        <span class="play-icon">▶</span>
                        <span class="pause-icon" style="display:none">⏸</span>
                    </button>
                    <button class="select-sound-btn" data-src="${sound.src}">Sélectionner</button>
                </div>
            `;
            libraryContainer.appendChild(soundElement);
            
            // Écouteur pour la prévisualisation du son
            const previewButton = soundElement.querySelector('.preview-sound-btn');
            const playIcon = previewButton.querySelector('.play-icon');
            const pauseIcon = previewButton.querySelector('.pause-icon');
            let audioPreview = null;
            
            previewButton.addEventListener('click', () => {
                if (!audioPreview) {
                    // Arrêter toutes les autres prévisualisations en cours
                    stopAllPreviews();
                    
                    // Créer un lecteur audio
                    audioPreview = new Audio(sound.src);
                    
                    // Mettre à jour l'information de source avec le son en cours de lecture
                    document.getElementById('sound-source-info').textContent = `En écoute: ${sound.name} (${sound.source || 'Source inconnue'})`;
                    
                    // Réinitialiser l'interface quand le son est terminé
                    audioPreview.onended = () => {
                        playIcon.style.display = 'inline';
                        pauseIcon.style.display = 'none';
                        
                        // Restaurer l'info de source
                        if (categoryFilter === 'all') {
                            document.getElementById('sound-source-info').textContent = 'Bibliothèque complète';
                        } else {
                            document.getElementById('sound-source-info').textContent = `Catégorie: ${categoryFilter}`;
                        }
                    };
                    
                    // Jouer le son
                    audioPreview.play();
                    playIcon.style.display = 'none';
                    pauseIcon.style.display = 'inline';
                } else if (audioPreview.paused) {
                    // Reprendre la lecture
                    audioPreview.play();
                    playIcon.style.display = 'none';
                    pauseIcon.style.display = 'inline';
                } else {
                    // Mettre en pause
                    audioPreview.pause();
                    playIcon.style.display = 'inline';
                    pauseIcon.style.display = 'none';
                }
            });
            
            // Écouteur pour la sélection d'un son
            soundElement.querySelector('.select-sound-btn').addEventListener('click', async () => {
                // Arrêter la prévisualisation si elle est en cours
                stopAllPreviews();
                
                await selectLibrarySound(sound.src, sound.name);
                libraryModal.style.display = 'none';
            });
        });
    } catch (error) {
        console.error('Erreur lors du chargement de la bibliothèque', error);
        libraryContainer.innerHTML = '<p>Impossible de charger la bibliothèque de sons.</p>';
    }
}

// Fonction pour arrêter toutes les prévisualisations en cours
function stopAllPreviews() {
    // Récupérer tous les boutons de prévisualisation
    const allPreviewButtons = document.querySelectorAll('.preview-sound-btn');
    
    // Réinitialiser chaque bouton à l'état par défaut
    allPreviewButtons.forEach(button => {
        button.querySelector('.play-icon').style.display = 'inline';
        button.querySelector('.pause-icon').style.display = 'none';
    });
    
    // Arrêter tous les éléments audio en cours de lecture
    document.querySelectorAll('audio').forEach(audio => {
        audio.pause();
    });
}

// Fonction pour sélectionner un son de la bibliothèque
async function selectLibrarySound(src, name) {
    try {
        progressContainer.style.display = 'block';
        updateProgressBar(0);
        
        let progress = 0;
        const processInterval = setInterval(() => {
            if (progress < 100) {
                progress += 20;
                updateProgressBar(progress);
            }
        }, 500);
        
        // Récupérer le fichier audio depuis le serveur
        const response = await fetch(src);
        const audioBlob = await response.blob();
        
        // Terminer la progression
        clearInterval(processInterval);
        updateProgressBar(100);
        
        // Créer l'élément audio
        createAudioClip(audioBlob, name);
    } catch (error) {
        console.error('Erreur lors de la sélection du son', error);
        alert('Le son n\'a pas pu être chargé. Veuillez réessayer.');
        
        // Réinitialiser la barre en cas d'échec
        updateProgressBar(0);
    } finally {
        progressContainer.style.display = 'none';
    }
}


// Bouton import ordinateur

fileInput.addEventListener('change', async (event) => {
    const file = event.target.files[0];
    if (file) {
        try {
            progressContainer.style.display = 'block';
            updateProgressBar(0);

            let progress = 0;
            const processInterval = setInterval(() => {
                if (progress < 100) {
                    progress += 20;  
                    updateProgressBar(progress);
                }
            }, 500); 

            // Traitement du fichier audio
            const audioBlob = await processImportedAudio(file);

            // Terminer la progression à 100% après le traitement
            clearInterval(processInterval);
            updateProgressBar(100);

            // Créer l'élément audio après l'importation
            createAudioClip(audioBlob, file.name);
        } catch (error) {
            console.error('Erreur lors de l\'import', error);
            alert('Votre audio n\'a pu être importé. Veuillez réessayer.');

            // Réinitialiser la barre en cas d'échec
            updateProgressBar(0);
        } finally{
            progressContainer.style.display = 'none';
        }
    }
});

async function processImportedAudio(file) {
    const arrayBuffer = await file.arrayBuffer();
    const audioContext = new (window.AudioContext || window.webkitAudioContext)();
    const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
    
    // Conversion du fichier importé en mp3
    const mp3Blob = await convertToMp3FromAudioBuffer(audioBuffer);
    return mp3Blob;
}

async function convertToMp3FromAudioBuffer(audioBuffer, progressCallback) {
    const channelData = audioBuffer.getChannelData(0); 
    return await optimizedMp3Encode(
        channelData,
        audioBuffer.sampleRate,
        1, 
        progressCallback
    );
}

// Mettre à jour la fonction createAudioClip pour toujours créer des clips MP3
function createAudioClip(blob, fileName) {
    const clipContainer = document.createElement('article');
    const clipLabel = document.createElement('p');
    const audio = document.createElement('audio');
    const deleteButton = document.createElement('button');
    const downloadButton = document.createElement('button');
    const moveUpButton = document.createElement('button');
    const moveDownButton = document.createElement('button');
    const splitButton = document.createElement('button');

    // Formatage du nom
    let clipName = fileName;
    if (fileName.includes('.')) {
        clipName = fileName.split('.').slice(0, -1).join('.');
    }

    clipContainer.classList.add('clip');
    audio.setAttribute('controls', '');
    
    // Configuration des boutons
    deleteButton.textContent = 'Supprimer';
    deleteButton.className = 'delete';
    downloadButton.textContent = 'Télécharger';
    downloadButton.className = 'download';
    moveUpButton.textContent = '↑';
    moveUpButton.className = 'move-up';
    moveDownButton.textContent = '↓';
    moveDownButton.className = 'move-down';
    splitButton.textContent = 'Découper ici';
    splitButton.className = 'split';
    splitButton.style.display = 'none';

    // Convertir le blob en MP3 si ce n'est pas déjà le cas
    const processBlob = async () => {
        if (!blob.type.includes('mp3')) {
            const tempAudio = new Audio();
            tempAudio.src = URL.createObjectURL(blob);
            
            await new Promise(resolve => {
                tempAudio.onloadedmetadata = resolve;
            });
            
            blob = await convertToMp3(tempAudio);
        }
        return blob;
    };

    processBlob().then(processedBlob => {
        const audioURL = URL.createObjectURL(processedBlob);
        audio.src = audioURL;
    });

    clipLabel.textContent = clipName || 'Clip Audio';

    // Gestion du bouton de découpage
    audio.addEventListener('pause', () => {
        if (isProMode) {
                splitButton.disabled = false;
                splitButton.dataset.splitTime = audio.currentTime;
        }
    });
    
    audio.addEventListener('play', () => {
        splitButton.disabled = true;
    });

    audio.addEventListener('ended', () => {
        if (isProMode) {
            splitButton.style.display = 'inline-block';
            splitButton.disabled = false;
            splitButton.dataset.splitTime = audio.duration;
        }
    });

    if (isProMode) {
        splitButton.style.display = 'inline-block';
    } else {
        splitButton.style.display = 'none';
    }
    
    splitButton.addEventListener('click', async () => {
        const splitTime = parseFloat(splitButton.dataset.splitTime);
        
        try {
            splitButton.disabled = true;
            //splitButton.textContent = 'Découpage en cours...';
            
            const [firstPart, secondPart] = await splitAudioAtTime(audio, splitTime);
            
            // Utilisation de generateSplitNames pour obtenir les nouveaux noms
            const baseName = clipLabel.textContent;
            const [firstPartName, secondPartName] = generateSplitNames(baseName);
            
            // Création des nouveaux clips avec les noms générés
            createAudioClip(firstPart, `${firstPartName}.mp3`);
            createAudioClip(secondPart, `${secondPartName}.mp3`);
            
            // Suppression du clip original
            clipContainer.remove();
            
        } catch (error) {
            console.error('Erreur lors du découpage:', error);
            alert('Erreur lors du découpage de l\'audio');
        } finally {
            splitButton.disabled = false;
            //splitButton.textContent = 'Découper ici';
        }
    });

    // Ajout des éléments au conteneur
    clipContainer.appendChild(moveUpButton);
    clipContainer.appendChild(moveDownButton);
    clipContainer.appendChild(clipLabel);
    clipContainer.appendChild(audio);
    clipContainer.appendChild(splitButton);
    clipContainer.appendChild(deleteButton);
    clipContainer.appendChild(downloadButton);
    soundClips.appendChild(clipContainer);

    // Gestion des événements des boutons
    deleteButton.onclick = function(e) {
        e.target.parentNode.remove();
    };

    clipLabel.onclick = function() {
        const newClipName = prompt('Entrez un nouveau nom pour votre clip audio');
        if(newClipName !== null) {
            this.textContent = newClipName;
        }
    };

    downloadButton.onclick = function() {
        const anchor = document.createElement('a');
        anchor.href = audio.src;
        anchor.download = `${clipLabel.textContent}.mp3`;
        anchor.click();
    };

    moveUpButton.onclick = function() {
        const previousSibling = clipContainer.previousElementSibling;
        if (previousSibling) {
            soundClips.insertBefore(clipContainer, previousSibling);
        }
    };

    moveDownButton.onclick = function() {
        const nextSibling = clipContainer.nextElementSibling;
        if (nextSibling) {
            soundClips.insertBefore(nextSibling, clipContainer);
        }
    };

    return clipContainer;
}

// ====================
// Combiner et Normaliser
// ====================

// Fonction pour le bouton Combiner
combineButton.onclick = async function() {
    const audioElements = document.querySelectorAll('.sound-clips .clip audio');
    
    if (audioElements.length < 2) {
        alert('Vous devez avoir au moins deux clips audio pour les combiner.');
        return;
    }

    const combinedClipName = prompt('Entrez un nom pour votre clip combiné', 'clipCombiné');
    if (!combinedClipName) return;

    // Création de la barre de progression
    const progressContainer = document.createElement('div');
    const progressBar = document.createElement('div');
    progressContainer.className = 'progress-container active';
    progressBar.className = 'progress-bar';
    progressContainer.appendChild(progressBar);
    combineButtonContainer.appendChild(progressContainer);
    
    try {
        combineButton.disabled = true;
        const audioContext = new (window.AudioContext || window.webkitAudioContext)();
        const mp3Blobs = [];

        // Conversion de chaque clip en MP3
        for (let i = 0; i < audioElements.length; i++) {
            const progress = (percent) => {
                const totalProgress = (i * 100 + percent) / audioElements.length;
                progressBar.style.width = `${totalProgress}%`;
            };
            
            const mp3Blob = await convertToMp3(audioElements[i], progress);
            mp3Blobs.push(mp3Blob);
        }

        // Concaténation des MP3
        const combinedBlob = new Blob(mp3Blobs, { type: 'audio/mp3' });
        const combinedUrl = URL.createObjectURL(combinedBlob);

        // Création du combiné
        const combinedClip = document.createElement('article');
        combinedClip.classList.add('clip');
        
        const combinedAudio = document.createElement('audio');
        combinedAudio.controls = true;
        combinedAudio.src = combinedUrl;
        
        const combinedLabel = document.createElement('p');
        combinedLabel.textContent = combinedClipName;
        
        const buttonsContainer = document.createElement('div');
        const downloadButton = document.createElement('button');
        const deleteButton = document.createElement('button');
        
        downloadButton.textContent = 'Télécharger';
        downloadButton.className = 'download';
        downloadButton.onclick = () => {
            const a = document.createElement('a');
            a.href = combinedUrl;
            a.download = `${combinedClipName}.mp3`;
            a.click();
        };
        
        deleteButton.textContent = 'Supprimer';

        deleteButton.onclick = () => {
            combinedClip.remove();
            URL.revokeObjectURL(combinedUrl);
        };
        
        buttonsContainer.appendChild(deleteButton);
        buttonsContainer.appendChild(downloadButton);
        deleteButton.className = 'delete';
        combinedClip.appendChild(combinedLabel);
        combinedClip.appendChild(combinedAudio);
        combinedClip.appendChild(deleteButton);
        combinedClip.appendChild(downloadButton);
        
        combinedClipsContainer.appendChild(combinedClip);

    } catch (error) {
        console.error('Erreur lors de la combinaison:', error);
        alert(`Erreur lors de la combinaison des clips: ${error.message}`);
    } finally {
        combineButton.disabled = false;
        progressContainer.remove();
    }
};

// Fonction pour calculer le RMS pour la normalisation
function calculateRMS(audioBuffer) {
    let sum = 0;
    for (let i = 0; i < audioBuffer.length; i++) {
        sum += audioBuffer[i] * audioBuffer[i];
    }
    return Math.sqrt(sum / audioBuffer.length);
}

// Fonction pour normaliser
async function normalizeAudioBuffer(audioContext, audioBuffer, targetRMS = 0.1) {
    const numberOfChannels = audioBuffer.numberOfChannels;
    const normalizedBuffer = audioContext.createBuffer(
        numberOfChannels,
        audioBuffer.length,
        audioBuffer.sampleRate
    );

    for (let channel = 0; channel < numberOfChannels; channel++) {
        const channelData = audioBuffer.getChannelData(channel);
        const currentRMS = calculateRMS(channelData);
        const gainFactor = targetRMS / currentRMS;

        const normalizedData = new Float32Array(channelData.length);
        for (let i = 0; i < channelData.length; i++) {
            normalizedData[i] = channelData[i] * gainFactor;
        }
        normalizedBuffer.copyToChannel(normalizedData, channel);
    }

    return normalizedBuffer;
}

// Fonction pour le bouton Normaliser
normalizeButton.onclick = async function() {
    const audioElements = document.querySelectorAll('.sound-clips .clip audio');
    
    if (audioElements.length < 2) {
        alert('Vous devez avoir au moins deux clips audio pour les combiner.');
        return;
    }

    const combinedClipName = prompt('Entrez un nom pour votre clip normalisé', 'clipNormalisé');
    if (!combinedClipName) return;

    // Création de la barre de progression
    const progressContainer = document.createElement('div');
    const progressBar = document.createElement('div');
    progressContainer.className = 'progress-container active';
    progressBar.className = 'progress-bar';
    progressContainer.appendChild(progressBar);
    combineButtonContainer.appendChild(progressContainer);
    
    try {
        normalizeButton.disabled = true;
        const audioContext = new (window.AudioContext || window.webkitAudioContext)();
        const audioBuffers = [];

        // Chargement et normalisation de chaque clip
        for (let i = 0; i < audioElements.length; i++) {
            const response = await fetch(audioElements[i].src);
            const arrayBuffer = await response.arrayBuffer();
            const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
            
            // Normalisation du buffer
            const normalizedBuffer = await normalizeAudioBuffer(audioContext, audioBuffer);
            audioBuffers.push(normalizedBuffer);
            
            progressBar.style.width = `${((i + 1) / audioElements.length) * 50}%`;
        }

        // Création du buffer final
        const totalLength = audioBuffers.reduce((sum, buffer) => sum + buffer.length, 0);
        const combinedBuffer = audioContext.createBuffer(
            1, // Mono output
            totalLength,
            audioContext.sampleRate
        );

        // Combinaison des buffers normalisés
        let offset = 0;
        for (const buffer of audioBuffers) {
            const channelData = buffer.getChannelData(0);
            combinedBuffer.copyToChannel(channelData, 0, offset);
            offset += buffer.length;
        }

        // Conversion en MP3
        const offlineContext = new OfflineAudioContext(
            1,
            combinedBuffer.length,
            combinedBuffer.sampleRate
        );
        
        const source = offlineContext.createBufferSource();
        source.buffer = combinedBuffer;
        source.connect(offlineContext.destination);
        source.start();

        const renderedBuffer = await offlineContext.startRendering();
        const mp3Blob = await optimizedMp3Encode(
            renderedBuffer.getChannelData(0),
            renderedBuffer.sampleRate,
            1,
            (progress) => {
                progressBar.style.width = `${50 + progress / 2}%`;
            }
        );

        // Création du clip combiné
        const combinedUrl = URL.createObjectURL(mp3Blob);
        const combinedClip = document.createElement('article');
        combinedClip.classList.add('clip');
        
        const combinedAudio = document.createElement('audio');
        combinedAudio.controls = true;
        combinedAudio.src = combinedUrl;
        
        const combinedLabel = document.createElement('p');
        combinedLabel.textContent = combinedClipName;
        
        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Supprimer';
        deleteButton.className = 'delete';
        
        const downloadButton = document.createElement('button');
        downloadButton.textContent = 'Télécharger';
        downloadButton.className = 'download';
        
        deleteButton.onclick = () => {
            combinedClip.remove();
            URL.revokeObjectURL(combinedUrl);
        };
        
        downloadButton.onclick = () => {
            const a = document.createElement('a');
            a.href = combinedUrl;
            a.download = `${combinedClipName}.mp3`;
            a.click();
        };

        combinedClip.appendChild(combinedLabel);
        combinedClip.appendChild(combinedAudio);
        combinedClip.appendChild(deleteButton);
        combinedClip.appendChild(downloadButton);
        
        combinedClipsContainer.appendChild(combinedClip);

    } catch (error) {
        console.error('Erreur lors de la normalisation et combinaison:', error);
        alert(`Erreur lors de la normalisation: ${error.message}`);
    } finally {
        normalizeButton.disabled = false;
        progressContainer.remove();
    }
};