**Nouveauté** : possibilité d'installer l'application sur tablette, téléphone et pc en tant que PWA. L'application sera alors accessible hors-ligne comme une application. 
**Attention** : cela n'est possible qu'en utilisant les navigateurs basés sur Chromium.


L'application est disponible [ici](https://combicast.forge.apps.education.fr/combicast/).


## Présentation de l'application
Cette application permet d'enregistrer des sons via le microphone de votre tablette, de votre téléphone ou de votre ordinateur, de réorganiser vos enregistrements, de les combiner et de les télécharger.

Les données ne sont pas stockées en ligne mais dans la mémoire locale du navigateur qui est temporaire. Si vous fermez la page ou la rafraichissez, vos enregistrements disparaissent. Votre vie privée est respectée.




### Réaliser un enregistrement

[Tutoriel](https://nuage03.apps.education.fr/index.php/s/nLqKM74fwfR786y)

Pour enregistrer cliquer sur <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_fa51f0896c1abd1c2e7312efa7501ce1.png" alt="placeholder" width="50" height="50">, un décompte apparaît.
Pour stopper l'enregistrement, il suffit de cliquer sur <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_49b4e49844854e13e4f58be84d948133.png" alt="placeholder" width="50" height="50">



### Vos enregistrements
Vous pouvez télécharger <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_d9c4fd43e5b738a6acd81f59ca40ebf3.png" alt="placeholder" width="50" height="50"> ou supprimer <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_7a20e54dfdfed4b21fb0d523678fab76.png" alt="placeholder" width="50" height="50">chaque enregistrement.
Une fois que vous avez plusieurs enregistrements, vous pouvez les réorganiser à l'aide des flèches
<img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_ca176720691ccc401dd8d5999130ecef.png" alt="placeholder" width="50" height="50"> et <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_eb8f64a59af8439b08cb11589dd7e7a3.png" alt="placeholder" width="50" height="50">

Vous pouvez les renommer en cliquant sur leur nom.
### Combiner 
Si vous avez au moins 2 enregistrements, vous pouvez les combiner pour ne former qu'un seul clip <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_62396822653ac7920728db3ce491d3bc.png" alt="placeholder" width="50" height="50">

### Mode PRO
Un clique sur ➕ vous permet d'accéder à un mode avec plus de fonctionnalités:

* <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_7dcc70b4c1f7fe5c9d263ab26d0619e6.png" alt="placeholder" width="50" height="50"> pour importer des sons

* <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_520c6c78651904483937f68184abb48e.png" width="50" height="50"> pour couper vos enregistrements à l'endroit de la pause. 2 fichiers seront créés.

* <img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_450ad6c241927b2f42dee910207dd778.png" width="50" height="50"> pour combiner vos clips


### Mode mobile
Le mode mobile 📱 est adapté à l'utilisation sur téléphone ou tablette.

<p>Il est possible d'installer l'application sur tablette, téléphone ou pc en tant que PWA (pour l'instant seulement pour les navigateurs basés sur chromium) pour une utilisation hors-ligne notamment en cliquant sur "Installer Combicast". Vous pouvez accéder à l'application pour mobile en scannant ce qrcode :</p>
            <br>
            <p><center><img src="https://nuage03.apps.education.fr/index.php/s/gqg4RgijaEcbtHs/preview" width="200"></a></center></p>
            <br>

### Utilisation hors-ligne
Vous pouvez aussi utiliser l'application hors-ligne et sur pc en téléchargeant [ce dossier](https://forge.apps.education.fr/combicast/combicast/-/archive/main/combicast-main.zip). Il suffit de décompresser le dossier et de lancer le fichier *index.html*.

## Autres logiciels
Vous pouvez utiliser [Logimix](https://ladigitale.dev/logimix/) pour faire des montages à partir de vos sons.

L'outil libre [Scribe des CEMÉA](https://scribe.cemea.org/) vous permet de transcrire vos sons en texte.

## La forge des communs numériques éducatifs
Ce [projet](https://forge.apps.education.fr/combicast/combicast) est disponible sur la forge des communs numériques éducatifs. Vous trouverez à [cette adresse](https://docs.forge.apps.education.fr) une liste non exhaustive des projets libres créés par la communauté ou [ici](https://primaire.forge.apps.education.fr/) une liste des outils dédiés à l'école primaire.
       
N'hésitez pas à laisser vos commentaires, remontées de bugs ou suggestions d'améliorations sur [l'espace du ticket](https://forge.apps.education.fr/combicast/combicast/-/issues) du projet. 
        

## Sources
Le projet original sous Licence CC0 est disponible à cette adresse [developper.mozilla.org](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_Recording_API/Using_the_MediaStream_Recording_API)

Le code de l'oscilloscope provient de [Sole](http://soledadpenades.com/).

Combicast est rendu possible grâce à [getUserMedia](https://developer.mozilla.org/en-US/docs/Web/API/Navigator.getUserMedia) (accès au micro),[MediaRecorder](https://developer.mozilla.org/en-US/docs/Web/API/MediaRecorder_API) (enregistrement) qui permettent de capturer facilement des flux média et [AudioBuffer](https://developer.mozilla.org/en-US/docs/Web/API/AudioBuffer) et [lamejs](https://github.com/zhuker/lamejs) - Encodeur MP3 en JavaScript sous licence GPL.

Les icones sont sous licences libres et proviennent du site [SVG REPO](https://www.svgrepo.com/)


Les IA ChatGPT et Claude ont participé à sa conception.

## Licence
Ce projet est distribué sous la licence **GNU General Public Licence**.<a href="https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU"><img src="https://upload.wikimedia.org/wikipedia/commons/9/93/GPLv3_Logo.svg" width="10%" height="10%">

